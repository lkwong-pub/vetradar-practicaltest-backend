# VetRadar Partical Test Backend

Design and write a headless catalog and shopping cart API in Javascript or Typescript.
The template provided uses @hapi as a basic REST framework. You don't need to use Hapi, if you feel it might save you time you can switch to
anything you'd like (even GraphQL), but out of
the interest of saving YOU time, we've bootstrapped the project and some of the REST calls.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

See deployment for notes on how to deploy the project on a live system.

Clone repository from GitLab
`git clone https://gitlab.com/lkwong-reference/vetradar-practicaltest-backend.git`



### Prerequisites

IDE
```
VSCode

```
DB 
```
N/A
```
Other
```
Docker, NPM
```

### Start application with Docker

A step by step series of examples that tell you how to get a development env running

1. Run
`docker-compose up` 

2. Open your browser and go to 
`http://localhost:3000/documentation`


### Start application with npm

A step by step series of examples that tell you how to get a development env running

1. Run
`npm start` 

2. Open your browser and go to 
`http://localhost:3000/documentation`


### Live Demo with Swagger

1. Open your browser and go to 
`https://vetradar-test.samaxxw.com/documentation#/`

## Built With

* [hapi](https://hapi.dev/) - The framework used

## Contributing

Not capable 

## Authors

* **Lai Kit Wong** - *Initial work*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Miscellaneous
```