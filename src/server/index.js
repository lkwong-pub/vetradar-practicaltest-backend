'use strict';

const Hapi = require('@hapi/hapi');
const Inert = require('@hapi/inert');
const Vision = require('@hapi/vision');
const Joi = require('@hapi/joi');
const HapiSwagger = require('hapi-swagger');
const Pack = require('package');

var productService = require('../service/productService')
var cartService = require('../service/cartService')


const init = async () => {

    const server = Hapi.server({
        port: 3000,
        host: '0.0.0.0'
    });

    server.route({
        method: 'GET',
        path: '/',
        options: {
            handler: (request, h) => {
                return productService.getData()
            },
            description: 'Get product list',
            notes: 'Return product list',
            tags: ['api', 'product'],
        },
    });

    server.route({
        method: 'GET',
        path: '/{id}',
        options: {
            handler: (request, h) => {
                return productService.findProductById(request.params.id, h)
            },
            description: 'Get product by id',
            notes: 'Return specific product',
            tags: ['api', 'product'],
            validate: {
                params: {
                    id : Joi.number()
                            .required()
                            .description('product id'),
                }
            }
     
        },
    });

    // Add product to cart
    server.route({
        method: 'POST',
        path: '/cart/{id}',
        options: {
            handler: (request, h) => {
                return cartService.addToCart(request, h)
            },
            description: 'Add product into the cart by id',
            notes: 'Return cart item list',
            tags: ['api', 'cart'],
            response: {
                status: {
                    201: Joi.any(),
                    400: Joi.any()
                }
            },
            validate: {
                params: {
                    id : Joi.number()
                            .required()
                            .description('Product id'),
                },
                payload: Joi.object({
                    qty: Joi.number()
                })
            }
        }
    });

    // Delete product from cart
    server.route({
        method: 'DELETE',
        path: '/cart/{id}',
        options: {
            handler: (request, h) => {
                return cartService.removeFromCart(request, h)
            },
            description: 'Remove product from cart by product id',
            notes: 'Return cart item list',
            tags: ['api', 'cart'],
            response: {
                status: {
                    200: Joi.any(),
                    400: Joi.any()
                }
            },
            validate: {
                params: {
                    id : Joi.number()
                            .required()
                            .description('Product id'),
                },
                payload: Joi.object({
                    qty: Joi.number()
                })
            }
        }
    });

    // Delete all products from cart
    server.route({
        method: 'DELETE',
        path: '/cart/all',
        options: {
            handler: (request, h) => {
                return cartService.removeAllFromCart(request, h)
            },
            description: 'Remove all products from cart',
            notes: 'NO content return',
            tags: ['api', 'cart'],
            response: {
                status: {
                    204: Joi.any(),
                    400: Joi.any()
                }
            }
        }
    });

    // View Cart data
    server.route({
        method: 'GET',
        path: '/cart',
        options: {
            handler: (request, h) => {
                return cartService.getCartData()
            },
            description: 'Show all cart items',
            notes: 'Return cart items',
            tags: ['api', 'cart'],
            response: {
                status: {
                    200: Joi.any(),
                    400: Joi.any()
                }
            }
        }
    });

    const swaggerOptions = {
        info: {
                title: 'VetRadar Practical Test - Shopping Cart API Documentation',
                version: Pack.version,
            },
        };

    await server.register([
        Inert,
        Vision,
        {
            plugin: HapiSwagger,
            options: swaggerOptions
        }
    ]);

    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

init();