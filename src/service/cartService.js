var dummyData = require('../models/product')
var cartData = require('../models/shoppingCart')

var productService = require('./productService')

module.exports = {

    addToCart: function (request, h) {

        var payload = request.payload
        var inputQty
    
        if (payload != null && Number.isInteger(payload.qty)) {
            var inputQty = payload.qty
        } 
    
        var qty = 1
        if (inputQty != null) {
            qty = inputQty
        }

        if (qty <= 0) {
            return h.response({ message : "invalid qty" }).code(400)
        }
        
        // find product by id
        var filteredArray = productService.findProductById(request.params.id)
    
        var productData = filteredArray[0]
          
        var existingCartProduct = this.findExistingProductById(request.params.id)
    
        if (existingCartProduct.length > 0) {
            // update element
            existingCartProduct[0].qty += qty
        } else {
            var cartItem = Object.assign({}, productData)
            cartItem.qty = qty
            cartData.cartItems.push(cartItem)
        }
    
        // if product exist append qty
        cartData.totalAmt = (parseFloat(cartData.totalAmt) + (productData.price * qty)).toFixed(2)
    
        cartData.totalQty += qty
    
        return h.response(cartData).code(201)
    },
    findExistingProductById: function(id) {

        var filteredArray = cartData.cartItems.filter(function(itm){
            return id == itm.id;
          });
        return filteredArray
    
    },

    removeFromCart: function(request, h) {

        var payload = request.payload
        var inputQty
        var qty = 1
    
        if (payload != null && Number.isInteger(payload.qty)) {
            var inputQty = payload.qty
            qty = inputQty
        } 

        if (qty <= 0) {
            return h.response({ message : "invalid qty" }).code(400)
        }
    
        // find product by id
        var filteredArray = productService.findProductById(request.params.id)
    
        var productData = filteredArray[0]
    
        // check existing product in cart
        var existingCartProduct = this.findExistingProductById(request.params.id)
        // if no throw error
    
        if (existingCartProduct.length < 1) {
            return h.response({ message : "item not found" }).code(400)
        }
        // if yes & qty > 1
        if (qty > existingCartProduct[0].qty) {
            return h.response({ message : "invalid qty" }).code(400)
        }
    
        if (existingCartProduct[0].qty > existingCartProduct[0].qty - qty && existingCartProduct[0].qty - qty != 0) {
            // do qty -1
            existingCartProduct[0].qty -= qty
        } else if (existingCartProduct[0].qty == qty || existingCartProduct[0].qty - qty <= 0) {
    
            console.log("remove item.")
            // else remove item
            cartData.cartItems = cartData.cartItems.filter(function(itm){
                return existingCartProduct[0].id != itm.id;
              });
    
        } else {
            return h.response({ message : "unhandled error" }).code(500)
        }
    
        // deduct the total
    
        cartData.totalAmt = (parseFloat(cartData.totalAmt) - (productData.price * qty)).toFixed(2)
    
        if (cartData.totalQty > 0) {
            cartData.totalQty -= qty
        }
    
        return h.response(cartData).code(200)

    }, 

    removeAllFromCart: function(request, h) {

        cartData = {
            cartItems: [],
            totalAmt: "0",
            totalQty: 0
        };
        return h.response().code(204)

    },
     
    getCartData: function() {
        return cartData
    }
}