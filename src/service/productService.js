var dummyData = require('../models/product')

module.exports = {
    findProductById: function(id, h) {

        var filteredArray = dummyData.filter(function(itm){
            return id == itm.id;
          });
    
        if (filteredArray.length == 0) {
            return h.response({message: "item not found."}).code(404)
        }
        return filteredArray
    
    },
    getData: function() {
        return dummyData
    }
}